//
//  GSTradeViewController.m
//  greenshark
//
//  Created by Marcelo Espina on 24-07-13.
//  Copyright (c) 2013 Acid Labs. All rights reserved.
//

#import "GSTradeViewController.h"

@interface GSTradeViewController ()

@end

@implementation GSTradeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setTitle:@"Trade"];
        [[self tabBarItem] setImage:[UIImage imageNamed:@"trade.png"]];
        
        [[self tabBarItem] setFinishedSelectedImage:[UIImage imageNamed:@"trade.png"]
                        withFinishedUnselectedImage:[UIImage imageNamed:@"trade.png"]];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
