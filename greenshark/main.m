//
//  main.m
//  greenshark
//
//  Created by Ricardo Andrés Bello on 7/11/13.
//  Copyright (c) 2013 Acid Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GSAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GSAppDelegate class]));
    }
}
