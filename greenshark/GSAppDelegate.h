//
//  GSAppDelegate.h
//  greenshark
//
//  Created by Ricardo Andrés Bello on 7/11/13.
//  Copyright (c) 2013 Acid Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UITabBarController *tabBarController;

@end
