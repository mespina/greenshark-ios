//
//  GSLoginViewController.h
//  greenshark
//
//  Created by Ricardo Andrés Bello on 7/11/13.
//  Copyright (c) 2013 Acid Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GSLoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;

- (IBAction)loginButtonPressed:(id)sender;
- (IBAction)signupButtonPressed:(id)sender;

@end
