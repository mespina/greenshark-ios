//
//  GSLoginViewController.m
//  greenshark
//
//  Created by Ricardo Andrés Bello on 7/11/13.
//  Copyright (c) 2013 Acid Labs. All rights reserved.
//

#import "GSLoginViewController.h"

@interface GSLoginViewController ()

@end

@implementation GSLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginButtonPressed:(id)sender {
}

- (IBAction)signupButtonPressed:(id)sender {
}
@end
