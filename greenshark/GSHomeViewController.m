//
//  GSHomeViewController.m
//  greenshark
//
//  Created by Marcelo Espina on 24-07-13.
//  Copyright (c) 2013 Acid Labs. All rights reserved.
//

#import "GSHomeViewController.h"

@interface GSHomeViewController ()

@end

@implementation GSHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        [self setTitle:@"Home"];
        [[self tabBarItem] setImage:[UIImage imageNamed:@"home.png"]];
        
        [[self tabBarItem] setFinishedSelectedImage:[UIImage imageNamed:@"home.png"]
                        withFinishedUnselectedImage:[UIImage imageNamed:@"home.png"]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
