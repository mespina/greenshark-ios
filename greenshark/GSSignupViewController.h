//
//  GSSignupViewController.h
//  greenshark
//
//  Created by Ricardo Andrés Bello on 7/11/13.
//  Copyright (c) 2013 Acid Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GSSignupViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *passwordConfirmationField;

- (IBAction)cancelButtonPressed:(id)sender;
- (IBAction)signupButtonPressed:(id)sender;

@end
